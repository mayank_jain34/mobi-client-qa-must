import webdriver from 'selenium-webdriver';
import config from '../config/config.js';

const By = webdriver.By;

export const getDriver = () => {
  let driver = new webdriver.Builder()
                            .forBrowser(config.forBrowser)
                            .build();
  driver.manage().window().maximize();                           
  return driver;
}

export const loginHelper = (driver) => {
  driver.get(config.url);
  let userNameField = driver.findElement(By.id('loginViewLoginIdInput'));
  let passwordField = driver.findElement(By.id('loginViewPasswordInput'));
  let submitbutton = driver.findElement(By.css('.btn.btn-info.btn-block'));
  
  userNameField.sendKeys(config.userName);
  passwordField.sendKeys(config.password);
  submitbutton.click();
}

export const logoutHelper = (driver) => {
  driver.findElement(By.xpath('//EM[@class="icon-user"]')).click();
  return driver.findElement(By.xpath('//A[@role="menuitem"][text()="Logout"]')).click();
}