import webdriver from 'selenium-webdriver';
import { getDriver } from './utils/utils.js';
import config from './config/config.js';

const By = webdriver.By;
const until = webdriver.until;
const Key = webdriver.Key;

describe('user login tests', () => {
  let driver = null;
  let url = config.url;
  let userName = config.userName;
  let password = config.password;
  
  beforeAll(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;
    driver = getDriver();
  });
  
  afterAll(() => {
    driver.close();
  });
  
  it('Invalid Login Test', (done) => {
    driver.get(url);
    let userNameField = driver.findElement(By.id('loginViewLoginIdInput'));
    let passwordField = driver.findElement(By.id('loginViewPasswordInput'));
    let submitbutton = driver.findElement(By.css('.btn.btn-info.btn-block'));

    let userData = [
      {username: 'foo', password: 'foo'},
      {username: 'bar@foo', password: 'foo'},
      {username: '_@foo', password: 'foo'},
      {username: 'foo@', password: 'foo'},
      {username: 'foo@bar.com', password: ''},
      {username: 'foo.bar@', password: 'foo'},
      {username: ' ', password: ' '}
    ]

    let loginTryPromiseArray = userData.map((user, index) => {
      return new Promise((resolve, reject) => {
          driver.sleep(2000).then(() => {
            userNameField.sendKeys(Key.CONTROL, "a", Key.DELETE, Key.NULL, user.username);
            passwordField.sendKeys(Key.CONTROL, "a", Key.DELETE, Key.NULL, user.password);
            submitbutton.isEnabled().then((isEnabled) => {
              isEnabled ? reject(user) : resolve();
            });
          });  
      });
    });

    Promise.all(loginTryPromiseArray)
    .then(() => {
      done();
    })
    .catch((data) => {
      done.fail(data);
    });
  });
  
  it('User Login Logout test', (done) => {
    driver.get(url);
    let userNameField = driver.findElement(By.id('loginViewLoginIdInput'));
    let passwordField = driver.findElement(By.id('loginViewPasswordInput'));
    let submitbutton = driver.findElement(By.css('.btn.btn-info.btn-block'));
    
    userNameField.sendKeys(userName);
    passwordField.sendKeys(password);
    submitbutton.click();
    
    driver.wait(until.elementLocated(By.className('icon-user')), 5000)
    .then(() => {
      let userIcon = driver.findElement(By.className('icon-user')).click();
      return userIcon.then(() => {
        driver.findElement(By.linkText('Logout')).click();
        return driver.wait(until.elementLocated(By.id('loginViewLoginIdInput')), 1000)
          .then(() => {
            done();
          });
      })
    })
    .catch(() => {
      done.fail();
    });
  });
  
});