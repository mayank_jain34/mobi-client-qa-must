import webdriver from 'selenium-webdriver';
import { getDriver, loginHelper, logoutHelper } from './utils/utils';
import config from './config/config.js';

const By = webdriver.By;
const until = webdriver.until;
const Key = webdriver.Key;
jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;

describe('promotizr tests', () => {
  var driver = null;
  let url = config.url;
  let userName = config.userName;
  let password = config.password;
  
  beforeEach(() => {
    driver = getDriver();
    loginHelper(driver);
  });
  
  afterEach((done) => {
    logoutHelper(driver).then(() => {
      driver.close();
      done();
    })
    
  });
  
  afterAll(() => {
    driver.close();
  });
  
  it("publish Modules: UI validation", (done) => {
    driver.wait(until.elementLocated(By.xpath('//SPAN[@data-localize="sidebar.nav.promotizr"][text()="Promotizr"]')), 20000)
    .then(() => {
      let promotizerSubmenu = driver.findElement(By.className('nav sidebar-subnav collapse in'));
      if(!promotizerSubmenu) {
        driver.findElement(By.xpath('//SPAN[@data-localize="sidebar.nav.promotizr"][text()="Promotizr"]')).click();
      }
      driver.findElement(By.xpath('//SPAN[@data-localize="sidebar.nav.promotizr.publishModules"][text()="Publish Modules"]')).click();
      driver.wait(until.elementLocated(By.xpath('//DIV[@class="content-heading"][text()="Publish Modules"]')), 20000)
      .then(() => {
        let publishModulesHeadingPromise = driver.findElement(By.xpath('//DIV[@class="content-heading"][text()="Publish Modules"]'))
        let importButtonPromise = driver.findElement(By.xpath('//BUTTON[@type="button"][text()="Import"]'));
        let exportAllButtonPromise = driver.findElement(By.xpath('//BUTTON[@type="button"][text()="Export All"]'));
        let exportSelectedButtonPromise = driver.findElement(By.xpath('//BUTTON[@type="button"][text()="Export Selected"]'));
        
        Promise.all([publishModulesHeadingPromise, importButtonPromise, exportAllButtonPromise, exportSelectedButtonPromise])
        .then((results) => {
          results.forEach(result => {
            expect(result).toBeDefined();
          });
          done();
        }).catch(done.fail);
      }).catch(done.fail);
    }).catch(done.fail);
  });
  
  describe('publish Modules: create new Module', () => {
    
    beforeEach((done) => {
      driver.wait(until.elementLocated(By.xpath('//SPAN[@data-localize="sidebar.nav.promotizr"][text()="Promotizr"]')), 20000)
      .then(() => {
        let promotizerSubmenu = driver.findElement(By.className('nav sidebar-subnav collapse in'));
        if(!promotizerSubmenu) {
          driver.findElement(By.xpath('//SPAN[@data-localize="sidebar.nav.promotizr"][text()="Promotizr"]')).click();
        }
        driver.findElement(By.xpath('//SPAN[@data-localize="sidebar.nav.promotizr.publishModules"][text()="Publish Modules"]')).click();
        driver.wait(until.elementLocated(By.xpath('//SPAN[@class="text"][text()="New Module"]')), 10000)
        .then(() => {
          driver.findElement(By.xpath('//SPAN[@class="text"][text()="New Module"]')).click();
          driver.wait(until.elementLocated(By.xpath('(//INPUT[@type="text"])[1]/following-sibling::INPUT')), 1000)
          .then(() => {  
            done();        
          }).catch(done.fail);
        }).catch(done.fail);
      }).catch(done.fail);
    });

    it('publish Modules: create new Module : Module Name Test', (done) => {
      driver.findElement(By.xpath('(//INPUT[@type="text"])[1]'))
      .then((moduleNameTextBox) => {
        moduleNameTextBox.sendKeys(Key.CONTROL, "a", Key.DELETE, Key.NULL);
        driver.sleep(200);
        driver.findElement(By.xpath('//SPAN[text()="Module name should not be empty."]'))
        .then(done)
        .catch(done.fail);
      })
      .catch(done.fail);
    });
    
    it('publish Modules: create new Module : Module Id Test', (done) => {
      let testStrings = ['', '`', '|', '!'];
      
      driver.findElement(By.xpath('(//INPUT[@type="text"])[2]'))
      .then((moduleNameTextBox) => {
        let promiseArray = testStrings.map((testString, index) => {
          return new Promise((resolve, reject) => {
              driver.sleep(200).then(() => {
                moduleNameTextBox.sendKeys(Key.CONTROL, "a", Key.DELETE, Key.NULL, testString)
                .then(() => {
                  driver.findElement(By.css('.field-errors.id'))
                  .then(() => {resolve(testString)})
                  .catch(() => {reject(testString)});
                });
              });  
          });
        });
    
        Promise.all(promiseArray)
        .then(() => {
          done();
        })
        .catch((data) => {
          done.fail(data);
        });
      }).catch(done.fail);
    });
    
    it('publish Modules: create new Module: buttons state check', (done) => {
      
      let saveButtonPromise = driver.findElement(By.xpath('//BUTTON[@type="button"][text()="Save"]')).isEnabled();
      let publishButtonPromise = driver.findElement(By.xpath('//BUTTON[@type="button"][text()="Publish"]')).isEnabled();
      let previewButtonPromise = driver.findElement(By.xpath('//BUTTON[@type="button"][text()="Preview (TBD)"]')).isEnabled();
      
      Promise.all([saveButtonPromise, publishButtonPromise, previewButtonPromise])
      .then((results) => {
        // if any of the button enable then fail the test case
        results.some(result => result) ? done.fail() : done();
      })
      .catch(done.fail);
    });
    
    it('publish Module: create new Module test', (done) => {
      driver.findElement(By.xpath('//DIV[@class="Select-placeholder"][text()="Select a list"]')).click();
      driver.sleep(200);
      driver.findElement(By.xpath('//DIV[@class="Select-menu-outer"]')).click();
      driver.findElement(By.xpath('//DIV[@class="Select-placeholder"][text()="Select a module"]')).click();
      driver.sleep(200);
      driver.findElement(By.xpath('//DIV[@class="Select-menu-outer"]')).click();
      driver.findElement(By.xpath('//BUTTON[@type="button"][text()="ADD"]')).click();
      
      let saveButtonPromise = driver.findElement(By.xpath('//BUTTON[@type="button"][text()="Save"]')).isEnabled();
      let publishButtonPromise = driver.findElement(By.xpath('//BUTTON[@type="button"][text()="Publish"]')).isEnabled();
      
      Promise.all([saveButtonPromise, publishButtonPromise])
      .then((results) => {
        results.some(result => result) ? done() : done.fail();
      })
      .catch(done.fail);
    });
  });

  /*xit('create Dynamic tile', (done) => {
    driver.wait(until.elementLocated(By.className('add-tile-btn')), 20000)
    .then(() => {
      driver.findElement(By.className('add-tile-btn')).click();
      driver.sleep(1000);
      driver.findElement(By.xpath('//div[@class="tile-type-cards"]/div[@class="tile-type-card"][1]')).click()
      driver.wait(until.elementLocated(By.xpath('//*[contains(text(), "Dynamic Tile Type")]'))).then(() => {
      driver.findElement(By.xpath('//*[contains(text(), "Select Dynamic Tile")]')).click();
      driver.sleep(500);
      driver.findElement(By.xpath('//select[@class="select2-hidden-accessible"]/option[3]')).click();
      driver.sleep(500);
      driver.findElement(By.xpath('//div[@class="save-preview-publish-buttons col-xs-12"]/button[contains(text(), "Create")]')).click().then(done);
      })
    });
  });*/
  
});