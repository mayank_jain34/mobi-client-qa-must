# README #

### What is this repository for? ###
This repo contains e2e test cases for MUST.


### How do I get set up? ###

 - Install all node modules by running command `'npm install'`
 - Add drivers available inside drivers directory into your environment path (these drivers are necessary for browser-based testing, for now, driver directory contains drivers for Chrome, Mozilla and IE you can add more if needed.)
 - Setup your configuration inside `'spec/config/config.js'` 
 - After completing above two steps run command `'npm start'`.

### How to write test cases? ###
 - Inside spec directory add your test case file by name ending with `'.spec.js;`
 - inside `'spec/config'` directory you can modify the config.js file to run your test cases in different browsers as well as the different environment.
