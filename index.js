var Jasmine = require('jasmine');
var jasmine = new Jasmine();
const JasmineConsoleReporter = require('jasmine-console-reporter');

jasmine.loadConfigFile('./jasmine.json');

jasmine.loadConfig({
  spec_dir: 'lib',
  spec_files: [
      "**/*[sS]pec.js"
  ],
  helpers: [
      'helpers/**/*.js'
  ],
  stopSpecOnExpectationFailure: false,
});

let consoleReporter = new JasmineConsoleReporter({
    colors: 1,           // (0|false)|(1|true)|2
    cleanStack: 1,       // (0|false)|(1|true)|2|3
    verbosity: 4,        // (0|false)|1|2|(3|true)|4
    listStyle: 'indent', // "flat"|"indent"
    activity: false
});

jasmine.addReporter(consoleReporter);

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

jasmine.execute();